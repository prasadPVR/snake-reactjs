import React, {useEffect, useState, useRef} from 'react';
import {useInterval, randomIntFromInterval} from './../../lib/utils'
import './Board.css'
const BOARD_SIZE = 15

class LinkNode {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}
class LinkedList {
    constructor(value) {
        const node = new LinkNode(value);
        this.head = node;
        this.tail = node;
    };
}
const fillBoard = size => {
    let counter = 1;
    const board = [];
    for(let row = 0; row < size; row++) {
        const currentRow = []
        for(let col = 0; col < size; col++) {
            currentRow.push(counter++);
        }
        board.push(currentRow);
    }
    return board;    
};

const getCellClassName = (cell, snakeCells, foodCell) => {
    let classname = "cell";
    if(snakeCells.has(cell)) {
        classname = "cell snake-cell";
    }
    if(foodCell === cell) {
        classname = "cell food-cell";
    }
    return classname;
}
const getStartingSnakePosition = board => {
    const rowSize = board.length;
    const colSize = board[0].length;
    const startingRow = Math.round(rowSize / 3);
    const startingCol = Math.round(colSize / 3);
    const cellValue = board[startingRow][startingCol];
    return {
        row: startingRow,
        col: startingCol,
        cell: cellValue
    }
}

const Direction = {
    UP : "UP",
    DOWN : "DOWN",
    LEFT : "LEFT",
    RIGHT : "RIGHT"
}

const getDirectionFromKeyPress = (key) => {
    if (key === 'ArrowUp') return Direction.UP;
    if (key === 'ArrowRight') return Direction.RIGHT;
    if (key === 'ArrowDown') return Direction.DOWN;
    if (key === 'ArrowLeft') return Direction.LEFT;
    return '';
}

const getNextHeadPositionWithDirection = (coordinate, direction) => {
    let newCoordinateAdd = {};
    switch (direction) {
        case Direction.UP : 
            newCoordinateAdd = {
                row : -1,
                col : 0
            };
            break;
        case Direction.DOWN : 
            newCoordinateAdd = {
                row : 1,
                col : 0
            };
            break;
        case Direction.RIGHT : 
            newCoordinateAdd = {
                row : 0,
                col : 1
            };
            break;
        case Direction.LEFT : 
            newCoordinateAdd = {
                row : 0,
                col : -1
            };
            break;
        default :
            newCoordinateAdd = {
                row : 0,
                col : 1
            };
            break;
    }

    return {
        row : newCoordinateAdd.row + coordinate.row,
        col : newCoordinateAdd.col + coordinate.col
    }
}

const getOppositeDirection = direction => {
    if (direction === Direction.UP) return Direction.DOWN;
    if (direction === Direction.RIGHT) return Direction.LEFT;
    if (direction === Direction.DOWN) return Direction.UP;
    if (direction === Direction.LEFT) return Direction.RIGHT;
};
const Board = props => {
    const [board, setBoard] = useState(fillBoard(BOARD_SIZE));

    const [score, setScore] = useState(0);
    const [snake, setSnake] = useState(new LinkedList(getStartingSnakePosition(board)));
    const [snakeCells, _setSnakeCells] = useState(new Set([snake.head.value.cell]));
    const [foodCell, setFoodCell] = useState(snake.head.value.cell + 5);
    const [direction, _setDirection] = useState(Direction.RIGHT);
    const directionHookRef = useRef(direction);
    const snakeCellsHookRef = useRef(snakeCells);

    const setDirection = direction => {
        directionHookRef.current = direction;
        _setDirection(direction);
    }

    const setSnakeCells = newSnakeCells => {
        snakeCellsHookRef.current = newSnakeCells;
        _setSnakeCells(newSnakeCells);
    }
    const handleKeydown = e => {
        const newDirection = getDirectionFromKeyPress(e.key);
        if(!newDirection) return;
        console.log( "new direction ",newDirection, directionHookRef.current);
        if(getOppositeDirection(directionHookRef.current) === newDirection && snakeCellsHookRef.current.size > 1) {
            return;
        }
        setDirection(newDirection);
    };
    useEffect(() => {
        window.addEventListener('keydown', e => {
          handleKeydown(e);
        });
      }, []);

    useInterval(() => {
        moveSnake()
    }, 300);

    const isValidCoordinate = coordinate => {
        const row = coordinate.row;
        const column = coordinate.col;

        const rowSize = board.length;
        const columnSize = board[0].length;
        if(row < 0 || row >= rowSize) {
            return false;
        }
        if(column < 0 || column >= columnSize) {
            return false;
        }
        return true;
    }
    const handleFoodConsumed = () => {
        while(true) {
            const newFoodCell = randomIntFromInterval(1,BOARD_SIZE*BOARD_SIZE);
            if(!snakeCells.has(newFoodCell)) {
                setFoodCell(newFoodCell);
                break;
            }
        }
        setScore(score + 1);
    }
    const moveSnake = () => {
        const currentSnakePositionCoord = {
            row : snake.head.value.row,
            col : snake.head.value.col
        }
        console.log( "Current direction ",direction)
        const nextHeadPositionCoord = getNextHeadPositionWithDirection(currentSnakePositionCoord, direction);
        if(!isValidCoordinate(nextHeadPositionCoord)) {
            handleGameOver();
            return;
        }
        nextHeadPositionCoord.cell = board[nextHeadPositionCoord.row][nextHeadPositionCoord.col];
        const nextHead = new LinkNode(nextHeadPositionCoord);
        snake.head.next = nextHead;
        snake.head = nextHead;

        const newSnakeCells = new Set(snakeCells);
        newSnakeCells.add(nextHeadPositionCoord.cell)

        const foodConsumed = nextHeadPositionCoord.cell === foodCell;

        if(foodConsumed === true) {
            handleFoodConsumed();
        }
        else {
            const snakeTail = snake.tail;
            snake.tail = snakeTail.next;
            newSnakeCells.delete(snakeTail.value.cell);
        }
        setSnakeCells(newSnakeCells);
    }

    const handleGameOver = () => {
        console.log("Game Over");
        const snakeLLStartingValue = getStartingSnakePosition(board);
        setSnake(new LinkedList(snakeLLStartingValue));
        setFoodCell(snakeLLStartingValue.cell + 5);
        setSnakeCells(new Set([snakeLLStartingValue.cell]));
        setDirection(Direction.RIGHT);
        setScore(0);
    }
    return (
        <div>
            <h1>Score : {score}</h1>
            <div className="board">
                {   
                    board.map((row,rowIndex) => {
                        return <div className="row" key={rowIndex}>
                            {
                                row.map((cell,cellIndex) => {
                                    {
                                        const cellClassName = getCellClassName(
                                            cell,
                                            snakeCells,
                                            foodCell
                                            );
                                        return <div className={cellClassName} key={cellIndex}></div>
                                    }
                                })
                            }
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default Board;